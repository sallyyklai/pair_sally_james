class Score_Calculator():
    def __init__(self):
        self.frame_index = 0
        self.bonus_record = {}
        self.total_score = 0

    def input_score(self,roll_count, score_for_frame=0):
        print("Current Frame :", self.frame_index)
        while True:
            score = int(input('Please input the score for roll-' + str(roll_count+1) + ': '))
            if score < 0 or score > 10:
                print("Please enter a score in the range 0 - 10")
            elif score_for_frame + score > 10:
                print("You can't score more than 10 in a frame!")
            else:
                break
        return score

    def processFrame(self):
        current_frame = self.frame_index
        score_for_frame = 0
        bonus_score = 0
        for i in range(2):
            score = self.input_score(i, score_for_frame)

            score_for_frame += score
            if score_for_frame == 10:
                self.bonus_record[current_frame] = 2 - i

            if (current_frame - 1) in self.bonus_record and self.bonus_record[current_frame - 1] > 0:
                self.bonus_record[current_frame - 1] -= 1
                bonus_score += score

            if (current_frame - 2) in self.bonus_record and self.bonus_record[current_frame - 2] > 0:
                self.bonus_record[current_frame - 2] -= 1
                bonus_score += score
            if score == 10:
                break
        return score_for_frame + bonus_score

    def processFinalFrame(self):
        current_frame = self.frame_index
        score_for_frame = 0
        bonus_score = 0
        for i in range(2):
            score = self.input_score(i)

            score_for_frame += score

            if score_for_frame == 10:
                self.bonus_record[current_frame] = 2 - i

            if (current_frame - 1) in self.bonus_record and self.bonus_record[current_frame - 1] > 0:
                self.bonus_record[current_frame - 1] -= 1
                bonus_score += score

            if (current_frame - 2) in self.bonus_record and self.bonus_record[current_frame - 2] > 0:
                self.bonus_record[current_frame - 2] -= 1
                bonus_score += score

        if self.bonus_record[current_frame] > 0:
            score = self.input_score(2)

        return score + bonus_score + score_for_frame

    def processNextFrame(self):
        while True:
            self.frame_index += 1
            if self.frame_index == 10:
                self.total_score += self.processFinalFrame()
                break
            else:
                score_frame = self.processFrame()
                self.total_score += score_frame
                print('Current Total Score is: ', self.total_score)
        print('Final Score is: ', self.total_score)

if __name__ == '__main__':
    cal = Score_Calculator()
    cal.processNextFrame()
